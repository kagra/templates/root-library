/**
 * @file THistogramTest.cpp
 * @brief A program demonstrating the usage of a custom histogram class with Doxygen comments.
 */

#include "MyRoot/THistogram.h" // Include your THistogram header file

#include <TPrint.h>

/**
 * @brief Main function to generate a histogram and test it.
 *  
 * @return 0 on successful execution.
 */
int main(int argc, char **argv) {

    // Create an instance of your custom histogram class THistogram
    MyRoot::THistogram *h = new MyRoot::THistogram("h", "Constant value histogram", 100, -5, 5);
    gPrint->Message("Histogram created with name: %s", h->GetName());

    // Fill the histogram with constant value
    int numberOfEntries = 10001;
    for (int i = 0; i < numberOfEntries; ++i) {
        h->Fill(0);
    }

    int N = h->GetEntries();
    return N != numberOfEntries;
}