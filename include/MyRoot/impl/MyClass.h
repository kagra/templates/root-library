#pragma once

#include <Riostream.h>
#include <TObject.h>
#include <vector>

namespace MyRoot {

    template <typename Element>
    class MyClass: public TObject {

        private:
            class Impl;
            Impl *pImpl;

        protected:
            std::vector<size_t> indices;

        public:

            MyClass();
            MyClass(const std::vector<size_t> &indices);
            virtual ~MyClass();

            template <typename... Indices>
            MyClass(Indices... indices): MyClass(std::vector<size_t>({indices...})) {}
            template<typename... Args>
            MyClass(const std::initializer_list<Args>&... args) : MyClass(std::vector<size_t>{static_cast<size_t>(*args.begin())...}) { }

            void Print();
            
        ClassDef(MyClass<Element>, 1);  // ROOT Class Definition
    };
}